# CaesarData

Code for performing and analysing CAESAR acquisitions at PSICHE beamline, Synchrotron SOLEIL.

This needs to handle the fact that acquisition runs in Python2.7 - for spyc.  The analysis should run in Python3 - for the rest of the world.

This should also be packaged better, separating the analysis and acquisition from a core part with common functionality.

Andrew King, PSICHE, 23/7/2021
