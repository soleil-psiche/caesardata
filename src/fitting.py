# -*- coding: utf-8 -*-
"""
some functions for fitting

Created on Thu Jun  5 19:25:11 2014

@author: aking
"""

from scipy.optimize import leastsq
import numpy as np
exp = np.exp
sin = np.sin

nonzero = np.nonzero
fft = np.fft.fft
ifft = np.fft.ifft


def fit_lorentzian(xdata, ydata):

    # fit a lorentzian function
    # y = A*((C/2)/((x-B)**2 + (C/2)**2)) + D
    # A = 
    # note C = FWHM
    optfunc = lambda x: (x[0]*((x[2]/2.)/((xdata - x[1])**2 + (x[2]/2.)**2))) + x[3] - ydata
    
    # guess the starting values
    A = ydata.max()-ydata.min()  
    B = xdata[nonzero(ydata==ydata.max())[0][0]]
    optfunc = lambda x: (x[0]*((x[2]/2)/((xdata - x[1])**2 + (x[2]/2)**2))) + x[3] - ydata
    D = ydata.min()
    C = (xdata[nonzero(ydata>D+(A/2))[0][-1]]-xdata[nonzero(ydata>D+(A/2))[0][0]])/4.
    
    # fit to the function
    x0 = [A, B, C, D]
    fitA, fitB, fitC, fitD = leastsq(optfunc, x0)[0]
    x1 = [fitA, fitB, fitC, fitD]   
    yfit = optfunc(x1) + ydata
    return x1, yfit

def fit_gaussian(xdata, ydata, negative=False):
    
    # fit a gaussian function
    #  y = A*exp(((x-B)/C)**2) + D
    optfunc = lambda x: (x[0]*exp(-((xdata-x[1])**2)/(2*(x[2]**2)))) + x[3] - ydata
    
    # guess the starting values
    if negative:
        A = ydata.min()-ydata.max()  
        B = xdata[nonzero(ydata==ydata.min())[0][0]]
        D = ydata.max()
        C = (xdata[nonzero(ydata<D+(A/2))[0][-1]]-xdata[nonzero(ydata<D+(A/2))[0][0]])/4.        
    else:
        A = ydata.max()-ydata.min()  
        B = xdata[nonzero(ydata==ydata.max())[0][0]]
        D = ydata.min()
        C = (xdata[nonzero(ydata>D+(A/2))[0][-1]]-xdata[nonzero(ydata>D+(A/2))[0][0]])/4.
    
    # fit to the function
    x0 = [A, B, C, D]
    fitA, fitB, fitC, fitD = leastsq(optfunc, x0)[0]
    x1 = [fitA, fitB, fitC, fitD]   
    yfit = optfunc(x1) + ydata
    return x1, yfit

def fit_gaussian_slope(xdata, ydata):
    
    # fit a gaussian function
    #  y = A*exp(((x-B)/C)**2) + D
    optfunc = lambda x: (x[0]*exp(-((xdata-x[1])**2)/(2*(x[2]**2)))) + x[3] + (xdata*x[4]) - ydata
    
    # guess the starting values
    A = ydata.max()-ydata.min()  
    B = xdata[nonzero(ydata==ydata.max())[0][0]]
    D = ydata.min()
    E = 0.
    C = (xdata[nonzero(ydata>D+(A/2))[0][-1]]-xdata[nonzero(ydata>D+(A/2))[0][0]])/4.
    
    # fit to the function
    x0 = [A, B, C, D, E]
    fitA, fitB, fitC, fitD, fitE = leastsq(optfunc, x0)[0]
    x1 = [fitA, fitB, fitC, fitD, fitE]   
    yfit = optfunc(x1) + ydata
    return x1, yfit

def correlate_1D(ydata, yref):
    """ assume same x data points"""

    # correlate the profiles
    corResult = ifft(fft(ydata)*np.conj(fft(yref)))
    corResult = np.real(corResult)
    c1 = np.nonzero(corResult == corResult.max())[0][0]
    
    # fit a quadratic through the peak
    ndx = np.mod(c1 + [-2, -1, 0, 1, 2], len(ydata))
    qy = corResult[ndx]
    #qx = xdata[ndx]
    qx = np.array([-2, -1, 0, 1, 2])
    qx = np.array([qx**2, qx, [1, 1, 1, 1, 1]]).T
    a,b,c = np.linalg.lstsq(qx, qy)[0]
    shift = (-b / (2*a)) + c1
    return shift


def fit_sine(xdata, ydata):

    # fit a sine wave, in DEGREES!
    #  y = A*sin(B+x)+C
    optfunc = lambda x: (x[0]*sin((np.pi/180)*(x[1]+xdata))) + x[2] - ydata
    
    # guess the starting values
    A = (ydata.max()-ydata.min())/2. 
    B = 0
    C = (ydata.max()+ydata.min())/2. 
    
    # fit to the function
    x0 = [A, B, C]
    fitA, fitB, fitC = leastsq(optfunc, x0)[0]
    x1 = [fitA, fitB, fitC]   
    yfit = optfunc(x1) + ydata
    return x1, yfit

def fit_caesar(xdata, ydata):

    # xdata is caesar theta, ydata is csho position
    # fit caesar X and S positions plus constant offset?
    # convert to radians
    xdata = xdata*np.pi/180
    optfunc = lambda x: -x[0]*sin(xdata) -x[1]*cos(xdata) + x[2] - ydata
    x0 = [0, 0, 0]
    
    # try to fit
    fitA, fitB, fitC = leastsq(optfunc, x0)[0]
    x1 = [fitA, fitB, fitC]
    yfit = optfunc(x1) + ydata
    print(str(x1))
    return x1, yfit
    # dmove caesardts1 negative x1[0], caesardtx1 negative x1[1]

def fit_exp(xdata, ydata):
    # fit exponential function y = a*np.exp(b*x)
    optfunc = lambda x: (x[0] * np.exp(x[1] * xdata)) - ydata
    # fit linear first to get gradient
    a,b = fit_linear(xdata, ydata)
    if a[0]>0:
        x0 = [1, 1]
    else:
        x0 = [1, -1]
    x1 =leastsq(optfunc, x0)[0]
    yfit = optfunc(x1) + ydata
    print(str(x1))
    return x1, yfit

def fit_linear(xdata, ydata):
    # fit simple linear function y = a*x + b
    optfunc = lambda x: (x[0] * xdata) + x[1] - ydata
    x0 = [1, 0]
    x1 =leastsq(optfunc, x0)[0]
    yfit = optfunc(x1) + ydata
    print(str(x1))
    return x1, yfit

def fit_poly(xdata, ydata, order=2):
    # fit n order poly y = a*x**2 + b*x + c
    if order==1:
        optfunc = lambda x: x[0] + x[1]*xdata - ydata
    elif order==2:
        optfunc = lambda x: x[0] + x[1]*xdata + x[2]*xdata**2 - ydata
    elif order==3:
        optfunc = lambda x: x[0] + x[1]*xdata + x[2]*xdata**2 + x[3]*xdata**3 - ydata
    elif order==4:
        optfunc = lambda x: x[0] + x[1]*xdata + x[2]*xdata**2 + x[3]*xdata**3 + x[4]*xdata**4 - ydata
    elif order==5:
        optfunc = lambda x: x[0] + x[1]*xdata + x[2]*xdata**2 + x[3]*xdata**3 + x[4]*xdata**4 + x[5]*xdata**5 - ydata
    else:
        print("sorry, 5 is the limit ! ")
        return
    x0 = np.zeros(order+1)
    x1 = leastsq(optfunc, x0)[0]
    yfit = optfunc(x1) + ydata
    return x1, yfit


def fit_gaussian_constrained(xdata, ydata, negative=False, fixed=[False, False, False, False], fixedval=[0, 0, 0, 0]):

    fixed = np.array(fixed)
    fixedval = np.array(fixedval)

    # fit a gaussian function
    #  y = A*exp(((x-B)/C)**2) + D

    # guess the starting values
    if negative:
        A = ydata.min()-ydata.max()  
        B = xdata[nonzero(ydata==ydata.min())[0][0]]
        D = ydata.max()
        C = (xdata[nonzero(ydata<D+(A/2))[0][-1]]-xdata[nonzero(ydata<D+(A/2))[0][0]])/4.        
    else:
        A = ydata.max()-ydata.min()  
        B = xdata[nonzero(ydata==ydata.max())[0][0]]
        D = ydata.min()
        C = (xdata[nonzero(ydata>D+(A/2))[0][-1]]-xdata[nonzero(ydata>D+(A/2))[0][0]])/4.

    # fit to the function
    x0 = np.array([A, B, C, D])

    # replace any fixed values
    x0[np.nonzero(fixed==True)] = fixedval[np.nonzero(fixed==True)]

    def optfunc(x):
        # force fixed values
        x = np.array(x)
        x[np.nonzero(fixed==True)] = fixedval[np.nonzero(fixed==True)]
        dif = (x[0]*exp(-((xdata-x[1])**2)/(2*(x[2]**2)))) + x[3] - ydata
        return dif 

    fitA, fitB, fitC, fitD = leastsq(optfunc, x0)[0]
    x1 = [fitA, fitB, fitC, fitD]   
    yfit = optfunc(x1) + ydata
    return x1, yfit

def fit_n_gaussians(xdata, ydata, n=2):
    
    x0 = np.zeros(3*n + 1) # three parameters per peak, plus single background
    for ii in range(n):
        x0[ii*3] = ydata.max()/2.
        x0[ii*3 + 1] = xdata.min() + (xdata.max()-xdata.min())*(ii*1.+1)/(n+1)
        x0[ii*3 + 2] = (xdata.max()-xdata.min())/20.
    x0[-1] = ydata.min()

    def optfunc(x, n):
        dif = (x[0]*exp(-((xdata-x[1])**2)/(2*(x[2]**2)))) + x[-1] - ydata
        if n>1:
            # add the other gaussians
            for ii in range(n):
                ndx = ii*3
                dif += (x[ndx+0]*exp(-((xdata-x[ndx+1])**2)/(2*(x[ndx+2]**2))))
        return dif

    x1 = leastsq(optfunc, x0, args=(n))[0]
    yfit = optfunc(x1, n) + ydata
    return x1, yfit


