# -*- coding: utf-8 -*-

'''
OK:  Make a class for caesar developments
This can inherit from the current caesar, and just adds what we want to test
Continue same idea for the multi channel version
'''
from CaesarDataMultiChannel import CaesarData as CaesarDataCore
# let's try to do this with matplotlib.pyplot
import matplotlib.pyplot as plt
import numpy as np
import sys
# suppress repeated warning messages
import warnings
warnings.filterwarnings(action='ignore', message='Mean of empty slice')
warnings.filterwarnings(action='ignore', message='invalid value encountered in true_divide')
warnings.filterwarnings(action='ignore', message='divide by zero encountered in true_divide')
warnings.filterwarnings(action='ignore', message='invalid value encountered in double_scalars')
print('Warning : Suppressing some warning messages (empty slice, invalid value in true divide)')

# something is strange
# even with simulated data, the first few channels of the tth correction seem to blow up.  Why is this?
# other than that it looks pretty good...
# must be something in the discretisation ? 

# don't understand everything, but it seems to converge for the lead glass example.
# need to make sure I can work with old (single channel) data

# approach of inheriting from a basic class seems good.

# current happy with this - I observe that it is pretty stable.  I don't get exactly the same results depending on
# the energy range chosen, and the number of iterations, but this may be due to imperfact data.
# The convergence test stops it from iterating too far, which is maybe good.
# 

# a question for another moment - fluorescence peaks are constant as function of angle.  Therefore, after pre-normalisation
# should they be constant ?
# could this be used for the volume correction part?


class CaesarData(CaesarDataCore):

    def __init__(self):
        ''' initialise CaesarData'''
        CaesarDataCore.__init__(self)
        # now we have all the normal stuff - just add the methods we want to test


    def amorphousDataTreatment(self):
        '''Try applying latest test version of treatment to Caesar acquistion'''
        if len(self.caesar_image) != 0:
            print("correcting the current Caesar data - development version of the code")
            self.__amorphousTreatment()
            self._CaesarData__showImage()
        else:
            print("Load Caesar data first!")


    def __amorphousTreatment(self, lowE=None, highE=None, Qnchan=None, defaults=None, Efactor=None, tthfactor=None, Enchan=None, exclude=None, niterations=None, conv=None):
        '''Convert every pixel into Q, allow volume correction if wanted'''

        # can we define a default option of 3x Efac=1, tthfac=0, then (niterations-3)x with Efac=0.1, tthfac=0.1
        print('This function can be used manually - or the default strategy can be used')
        print('Default does 3 iterations of E factor = 1, prenormalisation factor = 0, ')
        print('Then continues iterating with E factor = 0.2, prenormalisation factor = 0.1, ')
        print('until convergence criteria or max iterations is reached')


        # parameters
        print("Fit using a reduced energy range - ")
        print("Set the energy range. For the other parameters the defaults usually work well")
        if lowE == None:
            lowE = input("Starting energy [default 20] :")
            lowE = 20.0 if lowE == '' else float(lowE)
        if highE == None:
            highE = input("Final energy [default 60] :")
            highE = 60.0 if highE == '' else float(highE)
        if Qnchan == None:
            Qnchan = input('How many channels in Q (smooth data - 512? crystalline - 2048?) [1024] : ')
            Qnchan = 1024 if Qnchan == '' else int(Qnchan)
        if defaults == None:
            defaults = input('Use the default strategy? [y]/n : ')
            defaults = True if defaults.lower() in ['', 'y', 'yes', 'true'] else False
        if defaults:
            Efactor = None # these are controlled in the iteration loop
            tthfactor = None # these are controlled in the iteration loop
        else:
            if Efactor == None:
                Efactor = input('Factor for adjusting E spectrum (bigger is faster, smaller more stable) [0.2] : ')
                Efactor = 0.2 if Efactor == '' else float(Efactor)
            if tthfactor == None:
                tthfactor = input('Factor for adjusting the prenormalisation (bigger is faster, smaller more stable) [0.2] : ')
                tthfactor = 0.2 if tthfactor == '' else float(tthfactor)
        if Enchan == None:
            Enchan = input('Number of channels for E_eff [256] : ')
            Enchan = 256 if Enchan == '' else int(Enchan)
        if exclude == None:
            exclude = input("outlier rejection parameter in fitting (sigma, 3=amorphous, 0.2 xtalline)  [3] :")
            exclude = 3. if exclude == '' else float(exclude)
        if niterations == None:
            niterations = input('Max number iterations [50] : ')
            niterations = 50 if niterations == '' else int(niterations)
        if conv == None:
            conv = input('Convergence criteria (% change in std dev of difference) [0.1] : ')
            conv = 0.1 if conv == '' else float(conv)

        # specify channels to use...
        # take into account a (3D) mask 

        # issue here is that each pixel of the image now has 7 different Q values... 
        # this could be the logical place to combine channels ?
        # caesar_image.shape = (ntth, nenergy, nchans)
        # every pixel has:
        # channel (0-6)
        # Energy (calibrated energy)
        # tthstep (position in scan...)
        # tthval (calibrated tth value at given step)
        # Qvalue (calulated from tthval and energy)
        # Qchannel ? - defined relative to the limits of scan

        # do interpolation of energy to a common scale
        # interpolate all channels to the channel defined in self.__datachannel
        ### THIS PROBABLY SHOULD BE REMOVED
        myimage = np.zeros(self.caesar_image.shape)
        masterE = self.energy[self._CaesarData__datachannel, :] ### can continue to use this as the master

        # Global Q channel calculations
        # to start with, use extremes of caesar_tth
        Qmin = 4*np.pi*lowE*np.sin(np.deg2rad(self.caesar_tth.min()/2.))/12.398
        Qmax = 4*np.pi*highE*np.sin(np.deg2rad(self.caesar_tth.max()/2.))/12.398    

        # now start building grids, which are now 3D
        Egrid = np.zeros(self.caesar_image.shape)
        for ii in range(self.caesar_image.shape[0]):
            Egrid[ii, :, :] = self.energy.T # this respects the different Ecalib per channel

        # crop this (roughly) to lowE and highE
        ndx1 = np.nonzero(masterE>lowE)[0][0]
        ndx2 = np.nonzero(masterE>highE)[0][0]
        Egrid = Egrid[:, ndx1:ndx2, :]

        # make a grid for the E step as well
        # this need to respect the real E value
        ###Estepgrid = np.ones(Egrid.shape) * np.arange(Egrid.shape[1]).reshape((1, Egrid.shape[1], 1))
        # given that E is not necessarily linear, just discretise onto a reasonable number of points
        Estepgrid = np.round((Egrid - Egrid.min()) / (Egrid.max() - Egrid.min()) * (Enchan-1))
        # in some cases we only need 2d
        Estepgrid2d = Estepgrid[0, :, :]
        
        # same for tth, but with an step as well as the real value
        # for Q, we need the real value.  For normalising, it doesn't matter
        tthgrid = np.zeros(Egrid.shape) # this is the value
        tthstepgrid = np.zeros(Egrid.shape, int) # this is the caesar_step
        steps = np.arange(self.caesar_tth.shape[1]) # build a mesh
        steps = np.tile(steps.reshape(len(steps), 1), (1, self.caesar_image.shape[2]))
        for ii in range(tthgrid.shape[1]):
            tthgrid[:, ii, :] = self.caesar_tth.T
            tthstepgrid[:, ii, :] = steps

        # same for multichannel channel
        channelgrid = np.zeros(Egrid.shape)
        for ii in range(channelgrid.shape[2]):
            channelgrid[:,:,ii] = ii

        # calc Q for every pixel
        Qgrid = 4*np.pi*Egrid*np.sin(np.deg2rad(tthgrid/2.))/12.398
        # get I for every pixel from interpolated data
        #Igrid = myimage[:, ndx1:ndx2, :] * 1.
        Igrid = self.caesar_image[:, ndx1:ndx2, :] * 1. # get this directly from caesar_image, no interp.

        # get the mask too
        maskgrid = self.maskE[:, ndx1:ndx2, :]

        # before starting, should normalise between the channels - approximate only!
        # use only the un-masked pixels - important to get rid of nans
        print("Prenormalise between detector channels")
        ###never used : mask2D = maskgrid.min(2) # this is "worst case scenario"
        for ii in range(self.nchan):
            channelim = Igrid[:,:,ii]
            channelmean = channelim[np.nonzero(np.logical_not(np.isnan(channelim)))].mean()
            Igrid[:,:,ii] = Igrid[:,:,ii] / channelmean

        # Make Qchannels grid
        Qstep = (Qgrid.max()-Qgrid.min()) / (Qnchan-1)
        Qchannel = np.round((Qgrid - Qgrid.min()) / Qstep)
        Qchannel = Qchannel.astype(int)
        Qx = np.linspace(Qgrid.min(), Qgrid.max(), Qnchan)
        Qprofile_guess = np.zeros(Qx.shape)
        Qprofile_guess_grid = np.zeros(Qgrid.shape)

        # at this point, we've got Igrid, Egrid, tthgrid, tthstepgrid, Qgrid, Qchannels.
        # everything is 3D

        # to make Qprofile_guess and Qprofile_guess_grid efficently, make a LUT
        # this is a list of indices for each Q channel value
        # this gives us an efficent way to deal with mask
        print("Build LUTs...")
        Qtable = []
        for ii in range(Qnchan):
            Qtable.append(np.nonzero(np.logical_and(Qchannel==ii, maskgrid)))
        Etable = []
        for ii in range(Enchan):
            Etable.append(np.nonzero(np.logical_and(Estepgrid==ii, maskgrid)))
        # tth is a bit more complicated, because it is 2D
        # BUT IS IT REALLY? EACH ROW HAS TEH SAME NOMINAL TTH, EVEN THOUGH REAL TTH DEPENDS ON CHANNEL
        # so I think this is stupid - for tth we can just read across the rows
        # this can index into a 2d tth/E slice
        tthtable = []
        for jj in range(Igrid.shape[2]):
            tthsubtable = []
            for ii in range(Igrid.shape[0]):
                tthsubtable.append(np.nonzero(np.logical_and(tthstepgrid[:,:,jj]==ii, maskgrid[:,:,jj])))
            tthtable.append(tthsubtable)

        # record the evolution of the differences
        debug = True
        if debug:
            self.debug_ndx = np.arange(ndx1, ndx2)
            self.debug_Inorm = [] # list of normalised intensity at each iteration
            self.debug_dif1 = [] # list of difference at each iteration
            self.debug_Qprofile_guess = [] # list at each iteration
            self.debug_Qprofile_guess_grid = [] # list at each iteration
            self.debug_Eprofile_guess = [] # list at each iteration
            self.debug_Eprofile_adjust = [] # list at each iteration
            self.debug_Eprofile_adjust_npoints = [] # list at each iteration
            self.debug_tthprofile_guess = [] # list at each iteration
            self.debug_tthprofile_adjust = [] # list at each iteration
            
        # try starting without a guess - this is now the length that we chose above
        Eprofile_guess = np.ones((1, Enchan, 1))
        Eprofile_adjust = np.zeros(Eprofile_guess.shape)
        Eprofile_adjust_npoints = np.zeros(Eprofile_guess.shape)


        # tth profile different for each channel,but all start from ones
        tthprofile_guess = np.ones((Igrid.shape[0], 1, Igrid.shape[2])) # 2D
        tthprofile_adjust = np.zeros((Igrid.shape[0], 1, Igrid.shape[2])) # 2D

        # prepare a figure
        fig,axes = plt.subplots(3,2) # new figure with 3x2 axes
        axes[0,0].set_title('Qprofile_guess')
        axes[0,1].set_title('Convergence of differences')
        axes[1,0].set_title('Eprofile_guess')
        axes[1,1].set_title('Eprofile_adjust')
        axes[2,0].set_title('tthprofile_guess')
        axes[2,1].set_title('tthprofile_adjust')
        #fig2,axes2 = plt.subplots()
        #axes2.set_title('covergence of errors')
        mymap = plt.cm.Spectral(np.linspace(0,1,niterations))

        # Now, iterate to optimise 
        mystd_old = 1. # follow convergence
        mystd = 1.
        for iteration in range(niterations):

            # handle default strategy
            if defaults and iteration<3:
                myEfactor = 1.
                mytthfactor = 0.
            elif defaults:
                myEfactor = 0.2
                mytthfactor = 0.1
            else:
                myEfactor = Efactor
                mytthfactor = tthfactor

            # convert Eprofile_guess to 2d to account for channels
            Eprofile_guess2d = np.zeros((Igrid.shape[1], Igrid.shape[2]))
            for ii in range(Enchan):
                Eprofile_guess2d[np.nonzero(Estepgrid2d==ii)] = Eprofile_guess[0, ii, 0]
            Eprofile_guess2d = np.reshape(Eprofile_guess2d, (1, Igrid.shape[1], Igrid.shape[2]))
            
            # normalise using numpy magic array broadcasting :-)
            #Inorm = (Igrid / Eprofile_guess) * tthprofile_guess
            Inorm = (Igrid / Eprofile_guess2d) * tthprofile_guess

            # make the Qprofile_guess 
            for ii in range(Qnchan):
                Qprofile_guess[ii] = Inorm[Qtable[ii]].mean()
                Qprofile_guess_grid[Qtable[ii]] = Inorm[Qtable[ii]].mean()

            # the difference - normalise - 0 = good, 0.1 means 10% too high, -0.1 10% too low...
            dif1 = (Inorm - Qprofile_guess_grid) / Qprofile_guess_grid # seems to be better to normalise by the guess - more stable

            # show histo dif1
            a,b = np.histogram(dif1, 128, (-1,1))
            b = (b[1:]+b[0:-1])/2 # bin centres
            axes[0,1].plot(b, a, color=mymap[iteration, :])

            # save debug output
            if debug:
                self.debug_dif1.append(dif1*1.)
                self.debug_Inorm.append(Inorm*1.)
                self.debug_Qprofile_guess.append(Qprofile_guess*1.)
                self.debug_Qprofile_guess_grid.append(Qprofile_guess_grid*1.)
                self.debug_Eprofile_guess.append(Eprofile_guess*1.)
                self.debug_Eprofile_adjust.append(Eprofile_adjust*1.)
                self.debug_Eprofile_adjust_npoints.append(Eprofile_adjust_npoints*1.)
                self.debug_tthprofile_guess.append(tthprofile_guess*1.)
                self.debug_tthprofile_adjust.append(tthprofile_adjust*1.)

            # convergence test
            mystd_old = mystd * 1.
            mymean = (b*a).sum() / a.sum()
            mystd = ((a*((b-mymean)**2)).sum() / a.sum())**0.5
            change = 100*(mystd_old - mystd) / mystd_old
            sys.stdout.write('before iteration %d, Efac = %0.1f, tthfac = %0.1f, std = %0.3f, change = %0.1f\n' % (iteration, myEfactor, mytthfactor, mystd, change))
            sys.stdout.flush()
            if change < conv:
                if defaults and iteration<6:
                    print('in default config, do at least 6 iterations to adjust prenormalisation')
                else:
                    print('change %0.2f is below convergence criteria %0.2f' % (change, conv))
                    print('Break here to stop iterations !!!')
                    # highlight final results on plot
                    axes[0,1].plot(b, a, color='k')
                    axes[0,0].plot(Qprofile_guess, color='k')
                    axes[1,0].plot(Eprofile_guess[0,:,0], color='k')
                    axes[1,1].plot(Eprofile_adjust[0,:,0], color='k')
                    axes[2,0].plot(tthprofile_guess[:,0,:].mean(1), color='k')
                    axes[2,1].plot(tthprofile_adjust[:,0,:].mean(1), color='k')
                    plt.draw()
                    break

            # now - use this difference (as a f(energy)) to adjust Eprofile_guess
            Eprofile_adjust = np.zeros(Eprofile_guess.shape)
            Eprofile_adjust_npoints = np.zeros(Eprofile_guess.shape)
            for ii in range(Enchan):
                # do we have data?              
                if len(Etable[ii][0])>0:
                    # do outlier exclusion 
                    data = np.sort(dif1[Etable[ii]])
                    # clean inf and nan value
                    data = data[np.nonzero(np.logical_not(np.isinf(data)))]
                    data = data[np.nonzero(np.logical_not(np.isnan(data)))]
                    datamean = data.mean()
                    datastd = data.std()
                    ex1 = np.nonzero(data>(datamean-(datastd*exclude)))[0][0]
                    ex2 = np.nonzero(data<(datamean+(datastd*exclude)))[0][-1]
                    Eprofile_adjust[0,ii,0] = data[ex1:ex2].mean()
                    Eprofile_adjust_npoints[0,ii,0] = ex2-ex1
                    

            # limit adjustment ?
            amp = np.nanmax(np.abs(Eprofile_adjust))
            #print('E amp before limiting = %0.1f' % amp)
            if amp>myEfactor:
                Eprofile_adjust = Eprofile_adjust * myEfactor / amp

            # apply this to the E profile
            Eprofile_guess = Eprofile_guess * (1 + Eprofile_adjust)
            
            # normalise to max = 1
            Eprofile_guess = Eprofile_guess/Eprofile_guess.max()

            # now - use this difference (as a f(tth and channel)) to adjust tthprofile_guess
            #tthprofile_adjust = np.zeros(tthprofile_guess.shape)
            for jj in range(Igrid.shape[2]): # for each element
                for ii in range(Igrid.shape[0]):  # for each tth
                    # do we have data?              
                    if len(tthtable[jj][ii][0])>0:
                        # read the jj channel, and then the ii tthstep
                        data = np.sort(dif1[:,:,jj][tthtable[jj][ii]])
                        # clean inf and nan value
                        data = data[np.nonzero(np.logical_not(np.isinf(data)))]
                        data = data[np.nonzero(np.logical_not(np.isnan(data)))]
                        datamean = data.mean()
                        datastd = data.std()
                        ex1 = np.nonzero(data>(datamean-(datastd*exclude)))[0][0]
                        ex2 = np.nonzero(data<(datamean+(datastd*exclude)))[0][-1]
                        tthprofile_adjust[ii,0,jj] = data[ex1:ex2].mean()                

            # limit adjustment
            amp = np.nanmax(np.abs(tthprofile_adjust))
            #print('tth amp before limiting = %0.1f' % amp)
            if amp>mytthfactor:
                tthprofile_adjust = tthprofile_adjust * mytthfactor / amp
                
            # apply this to the tth profile - we divide by tthprofile, so change sign here
            # positive sign seems to give right behavior - guess too high, dif negative, adjust negative, guess gets smaller?
            # but negative seems to remove stripes from the difference
            tthprofile_guess = tthprofile_guess * (1 - tthprofile_adjust)
            
            # normalise to mean = 1
            tthprofile_guess = tthprofile_guess/tthprofile_guess.mean()

            if True: 
                axes[0,0].plot(Qprofile_guess, color=mymap[iteration, :])
                axes[1,0].plot(Eprofile_guess[0,:,0], color=mymap[iteration, :])
                axes[1,1].plot(Eprofile_adjust[0,:,0], color=mymap[iteration, :])
                axes[2,0].plot(tthprofile_guess[:,0,:].mean(1), color=mymap[iteration, :])
                axes[2,1].plot(tthprofile_adjust[:,0,:].mean(1), color=mymap[iteration, :])
                plt.draw()

        sys.stdout.write('\ndone!\n\n')

        # make the final output
        for ii in range(Qnchan):
            Qprofile_guess[ii] = Inorm[Qtable[ii]].mean()
            Qprofile_guess_grid[Qtable[ii]] = Inorm[Qtable[ii]].mean()

        # put results back into self
        self.fitted_spectrum = Eprofile_guess.flatten()
        self.fitted_spectrum_energy = masterE[ndx1:ndx2] # includes the crop
        self.fitted_volume_correction = tthprofile_guess # keep a copy of this
        self.caesar_image_original = self.caesar_image * 1. # backup the initial image
        imnew = np.zeros(self.caesar_image.shape)
        imnew[:, ndx1:ndx2, :] = Inorm
        self.caesar_image = imnew
        self._CaesarData__convertImage() # this gives an image in d as well
        self._CaesarData__showImage()
        # use my_plot to show the result of the optimisation
        my_plot3(self.caesar_image_Q, self.caesar_image_Qx, self.caesar_tth_nominal) 
        # dump out some useful stuff
        self.debug_Qprofile_guess = Qprofile_guess # the actual fitted profile
        self.debug_Qprofile_guessx = Qx # the actual fitted profile x
        self.debug_Qgrid = Qgrid # the Q value per voxel
        self.debug_Igrid = Igrid # the initial raw intensity per voxel
        self.debug_maskgrid = maskgrid # the mask value per voxel
        self.debug_Qchannel = Qchannel # the Q channel per voxel
        self.debug_Qtable = Qtable # the table to link voxel to Q channel
        self.debug_Etable = Etable # the table to link voxel to E channel
        #self.debug_tthtable = tthtable # the table to link voxel to tth channel

        

def my_plot3(imQ, Qx, tth, nprofiles=8):
    fig, axes = plt.subplots(2,1)
    smallimQ = imQ[:, ::5, :] # take 1 in 5 points in Q... still lots
    smallQx = Qx[::5]
    lines = np.arange(0, imQ.shape[0], int(imQ.shape[0]/nprofiles))
    lines[-1] = imQ.shape[0]-1
    mymap = plt.cm.Spectral(np.linspace(0,1,len(lines)))
    # take all the voxels for a slice (tth-chan) of the Qimage, 
    for ii,ndx in enumerate(lines):
        axes[0].plot(np.tile(smallQx.reshape((len(smallQx), 1)), (1, smallimQ.shape[2])).flatten(), smallimQ[ndx, :, :].flatten(), '+', color=mymap[ii, :], label='tth %0.1f'%tth[ndx])       
    axes[0].legend()

    axes[0].set_title('Consistency between angles, for all channels')
    mymap = plt.cm.Spectral(np.linspace(0,1,imQ.shape[2]))
    smallimQ = imQ[::5, ::5, :] # take 1 in 5 points in tth and Q... still lots
    smallQx = Qx[::5]
    # take all the voxels for a channel (tth-Q) of the Q image
    for chan in np.arange(imQ.shape[2]):
        axes[1].plot(np.tile(smallQx.reshape((1, len(smallQx))), (smallimQ.shape[0], 1)).flatten(), smallimQ[:,:,chan].flatten(), '+', color=mymap[chan, :], label='channel %d'%chan)       
    axes[1].legend()
    axes[1].set_title('Consistency between channels, for all angles')




#def myConvertImage(im, tth, energy, test_norm=False):
    ''' convert data from energy to dspacing and Q 
        return Nan outside the energy range'''
    '''# convert the spectrum, if available
    theta = tth/2
    # preallocate an output image
    npoints = 4096
    im2 = np.zeros([im.shape[0], npoints])
    # dspacing limits for the whole acquistion
    dmax = 12.398 / (2*np.sin(theta[0]*np.pi/180)*energy[0]) # lowest angle, lowest energy
    dmin = 12.398 / (2*np.sin(theta[-1]*np.pi/180)*energy[-1]) # highest angle, highest energy            
    Qmin = 2*np.pi/dmax
    Qmax = 2*np.pi/dmin
    Qgrid = np.linspace(Qmin, Qmax, npoints)
    # convert line by line
    for ii in range(im.shape[0]):
        # energy profile
        prof = im[ii, :]
        # as d spacing                
        dsp = 12.398 / (2*np.sin(theta[ii]*np.pi/180)*energy)
        # as Q
        Q = 2*np.pi/dsp
        if test_norm:
            print('test normalising') # these two factors largely compensate for each other.
            prof = prof / (Q[-1]-Q[0])
            prof = prof / np.arctan(10./(1220*np.sin(np.deg2rad(tth[ii]))))
        # as Q - flip profiles for increasing
        im2[ii, :] = np.interp(Qgrid, Q, prof, np.nan, np.nan)    

    return im2, Qgrid
    '''

#def myMakeProfileIm2(im, axis=0, exclude=3):
    '''
    make nan mean profile, but excluding extreme values (defined in sigma)
    '''
    '''count = (np.logical_not(np.isnan(im))).sum(axis)
    p = np.ones(im.shape[1-axis]) * np.nan
    for ii in range(im.shape[1-axis]):
        if count[ii]>1:
            if axis==0:
               data = im[:, ii]
            else:
                data = im[ii, :]
            data =  np.sort(data) # sort into ascending order, nans at the end
            data = data[0:count[ii]] # remove nans
            mystd = data.std()
            mymean = data.mean()
            ndx1 = np.nonzero(data>(mymean-(mystd*exclude)))[0][0]
            ndx2 = np.nonzero(data<(mymean+(mystd*exclude)))[0][-1]
            p[ii] = data[ndx1:ndx2].mean()
    if axis==0:
        pim = np.tile(p, (im.shape[0], 1))
    else:
        pim = np.tile(p.reshape(len(p), 1), (1, im.shape[1]))
    return pim
'''

#def myMakeProfileIm(im, axis=0):
    '''
    make nan mean profile image for normalising
    '''
    '''p = np.nansum(im, axis)
    count = (np.logical_not(np.isnan(im))).sum(axis)
    p = p/count
    if axis==0:
        pim = np.tile(p, (im.shape[0], 1))
    else:
        pim = np.tile(p.reshape(len(p), 1), (1, im.shape[1]))
    return pim
'''

#def myMakeProfile(im, axis=0):
#    '''
#    make nan mean profile for normalising
#    '''
#    p = np.nansum(im, axis)
#    count = (np.logical_not(np.isnan(im))).sum(axis)
#    p = p/count
#    return p


#def my_plot(qimage, Qgrid, caesar_tth, colour=None, npoints=7):
#    ndx = np.arange(0, qimage.shape[0], np.round(qimage.shape[0]/npoints))
#    if colour != None:
#        for ii in range(len(ndx)):
#            pylab.plot(Qgrid, qimage[ndx[ii],:], colour, label=('two theta %0.1f' % caesar_tth[ndx[ii]]))
#    else:
#        for ii in range(len(ndx)):
#            pylab.plot(Qgrid, qimage[ndx[ii],:], label=('two theta %0.1f' % caesar_tth[ndx[ii]]))
#    pylab.legend()

#def my_plot2(Inorm, Qlist, Elist, tthchannel, tthnominal):
#    channel = 3
#    ndx = np.arange(0, len(tthnominal)+0.1, int(np.round(len(tthnominal)/5)))
#    for ii in ndx:
#        pylab.plot(Qlist[np.nonzero(tthchannel==ii)], Inorm[np.nonzero(tthchannel==ii)], '+', label=('two theta %0.1f' % tthnominal[ii]))
#    pylab.legend()


        



