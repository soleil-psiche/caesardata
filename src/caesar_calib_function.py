#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 23 11:51:34 2023

@author: andrewking
"""

import glob
import matplotlib.pyplot as plt
import pandas
import numpy as np
from scipy.optimize import leastsq
from tkinter import filedialog
import os

def doCalib():
    
    # get to the directory with the data
    curdir = os.getcwd()
    print('Select the directory with the calibration data')
    mydir = filedialog.askdirectory(initialdir=curdir)
    mydir = curdir if mydir == '' else mydir
    os.chdir(mydir)
    mylist = glob.glob('*.x_y')
    # do something sensible with this list - sort, parse names
    # we have elements, angles, channels
    mydata = parse_list(mylist)
    
    #Open a recent calib as a starting point
    print('Open a recent calibration file as a starting point')
    print('Cancel to start from defaut values')
    rough_calib_filename = filedialog.askopenfilename(initialdir=mydir)
    if rough_calib_filename == '':
        calib = getDefaultCalib()
    else:
        calib = np.loadtxt(rough_calib_filename, skiprows=1, delimiter=',')
    
    # parameters
    # use this range around the peaks
    fit_range = 50
    # calibE order
    order = 2
    # distance for angle
    dist = 1180
    
    # first, calibrate energy
    calib = makeCalibE(mydata, calib, order, fit_range)
        
    # now, using the updated energy calibration, calibrate angle
    calib = makeCalibA(mydata, calib, dist, fit_range)
    
    # save the file
    saveCalib(calib)

    
def makeCalibA(mydata, calib, dist, fit_range=50):
    # what elements do we have?
    elements = np.unique(mydata['element'])
    chans = np.unique(mydata['channel'])
    print('Found the following elements: %s' % str(elements))
    element = input('Use which element for angle calibration? [Au] : ')
    element = 'Au' if element == '' else element
    
    # reduce mydata to make later stuff easier
    elementdata = mydata.loc[mydata['element']==element]
    angles = np.unique(elementdata['angle'])
    print('Will use the following angles: %s' % str(angles))
    
    # get the peaks (in dsp) for this element
    all_peaks,all_hkls = getDiffData(element)
    
    # calibs
    calibE = calib[:,1:4]
    calibA = calib[:,4:7]
    
    # save fitted data
    fitted_data = np.zeros((calib.shape[0], len(angles)))
    
    # make a figure
    fig= plt.figure()
    
    # get the channel range for xdata (25 - 75 keV)
    #tmp = energy2channel(np.array((25,75))*1000, calibE)
    #xdata = np.arange(int(tmp.min()), int(tmp.max()))
    
    # run through the elements
    for jj,angle in enumerate(angles):

        # guess the effective tths from the rough calib        
        tth_guess = estimate_tth(angle, calibA, dist)        
        tth_fitted = np.zeros(tth_guess.shape)

        # variables for figure elements
        global axes
        axes = list(np.zeros(len(chans))) 
        lines = list(np.zeros(len(chans)))
        markers = list(np.zeros(len(chans)))
        fits = list(np.zeros(len(chans)))    

        # we want to take the peaks (dsp) and calib (E) and fit theta for each pattern
        # convert peaks to rough channel values
        # get the hkl list for this angle        
        peaks, hkls = select_peaks_dsp(all_peaks, all_hkls, calib, dist, angle)
        print(hkls)
        peaks_channel = np.zeros((calib.shape[0], len(peaks)))
        # populate this with nominal values
        for ii in range(calib.shape[0]):
            peaks_channel[ii, :] = dsp2channel(peaks, tth_guess[ii], calibE[ii, :].reshape((1,3)))

        xmin = peaks_channel.min() - fit_range
        xmax = peaks_channel.max() + fit_range
        xdata = np.arange(int(xmin), int(xmax))

        # clear the figure to start with
        fig.clf()          
    
        # add a subplot for each channel - we should always have all channels... maybe this is over complicating?
        for ii,chan in enumerate(chans):
            
            # make subplot
            axes[ii] = fig.add_subplot(len(chans),1,ii+1)
            if ii == 0:
                axes[0].set_title('click to adjust fits - hold shift to force position')
            # find the data
            ndx = np.where(np.logical_and(elementdata['angle']==angle, elementdata['channel']==chan))[0]
            if len(ndx)==1:
                # read the profile
                prof = np.loadtxt(elementdata.iloc[ndx[0]]['filename'])
                prof = prof[xdata,1]
            elif len(ndx)>1:
                print('found more than one profile for %s, angle %d, channel %d' % (element, angle, chan))
                # average them
                if True:
                    prof = np.loadtxt(elementdata.iloc[ndx[0]]['filename'])
                    prof = prof[xdata,1]
                    for myndx in ndx[1:]:
                        tmp = np.loadtxt(elementdata.iloc[myndx]['filename'])
                        prof += tmp[xdata,1]
                else:
                    # take the first one only
                    ndx = ndx[0] # take the first one
            elif len(ndx)==0:
                print('No profile found for %s, angle %d, channel %d' % (element, angle, chan))

            # plot this
            lines[ii] = axes[ii].plot(xdata, prof, label='%d deg ch %0.1f' % (angle, tth_guess[ii]))
            axes[ii].legend(loc='right')
            axes[ii].set_ylim(0, prof.max()*1.2)
            # plot the starting points
            markers[ii] = axes[ii].plot(peaks_channel[ii,:], np.ones(len(peaks))*prof.max(), 'r*')
            # try to fit based on the automatic values
            fitvalues, fitprofile = fitPeaksAngle(tth_guess[ii], peaks, calibE[ii,:].reshape((1,3)), xdata, prof) # note [] so len = 1
            # add to the plot
            fits[ii] = axes[ii].plot(xdata, fitprofile, 'r-')
            # this gives us a first attempt
            tth_fitted[ii] = fitvalues[0]
            # handle cases where we cant fit - to add (hold shift and click?)
    
            
        # interactive update of markers
        loop = True
        print('click on the profiles to adjust the starting point for the fits - when finished, click outside the axes')
        global myaxis
        global coords
        global shift
        coords = []
        myaxis = None
        shift = False
        plt.draw()
        while myaxis != -1:
            cid = fig.canvas.mpl_connect('button_press_event', clickfunc)
            cid2 = fig.canvas.mpl_connect('key_press_event', keypressfunc)
            cid2 = fig.canvas.mpl_connect('key_release_event', keyreleasefunc)
            print(cid)
            print('in update')
            if not plt.waitforbuttonpress():
                print('here I am... '+str(myaxis))
                if myaxis == -1:
                    # disconnect from the figure
                    print('disconnect from the figure')
                    fig.canvas.mpl_disconnect(cid)
                    break
                # modify peaks_channel
                offset = coords[0] - peaks_channel[myaxis, :]
                offset_abs = np.abs(offset)
                ndx = np.nonzero(offset_abs == offset_abs.min())[0]
                oldvalue = peaks_channel[myaxis, ndx]     
                # use this adjust tth_guess
                tth_guess[myaxis] = modify_tth_guess(tth_guess[myaxis], coords[0], oldvalue, calibE[myaxis,:])
                peaks_channel[myaxis, :] = dsp2channel(peaks, tth_guess[myaxis], calibE[myaxis, :].reshape((1,3)))
                # update the starting points
                markers[myaxis][0].set_xdata(peaks_channel[myaxis,:])
                # refit from this point
                prof = lines[myaxis][0].get_ydata()
                xdata = lines[myaxis][0].get_xdata()
                fitvalues, fitprofile = fitPeaksAngle(tth_guess[myaxis], peaks, calibE[myaxis,:].reshape((1,3)), xdata, prof, shift) # note [] so len = 1
                # add to the plot
                fits[myaxis][0].set_ydata(fitprofile)
                plt.draw()
                # save this updated value
                tth_fitted[myaxis] = fitvalues[0]
                # handle cases where we cant fit
        # collect the fitted data
        fitted_data[:, jj] = tth_fitted
    # use this to fit the angle and geometry calib
    calibA = makeCalibAngle(fitted_data, angles, dist) 
    calib[:,4:7] = calibA
    
    # close the figure
    plt.close(fig)

    # display the data
    plt.figure()
    for ii in range(len(fitted_data[1])):
        plt.plot(fitted_data[:, ii] - fitted_data[:, ii].mean(), '-x',label = 'angle '+str(int(angles[ii]))+'deg')
    plt.title('fitted angle per channel, for each nominal angle')
    plt.legend()
    return calib
    # end of the main function
            
    
    
   
def makeCalibE(mydata, calib, order=2, fit_range=50):
    
    # what elements do we have?
    elements = np.unique(mydata['element'])
    chans = np.unique(mydata['channel']) 


    print("Channel should start at "+str(chans[-1]))
    print('found the following elements: %s, for %d channels' % (str(elements), len(chans)))
    
    # save fitted data
    fitted_data = {}
    
    # make a figure
    fig= plt.figure()
    
    # run through the elements
    for element in elements:
        
        # clear the figure to start with
        fig.clf()
    
        # variables for figure elements
        global axes
        axes = list(np.zeros(len(chans))) 
        lines = list(np.zeros(len(chans)))
        markers = list(np.zeros(len(chans)))
        fits = list(np.zeros(len(chans)))
        
        # get the fluo data so we know where to select
        peaks = getFluoData(element)
        
        # convert these to rough channel numbers
        # use the energy part of the rough calib
        peaks_channel = energy2channel(peaks, calib[:,1:4])
        peaks_channel_fitted = np.zeros(peaks_channel.shape)
        xmin = int(peaks_channel.min()-fit_range)
        xmax = int(peaks_channel.max()+fit_range)
        xdata = np.arange(xmin, xmax)
        
        # save this
        fitted_data.update({element:{'peaks':peaks, 'first_guess':peaks_channel, 'fit':peaks_channel_fitted}})
        
        # add a subplot for each channel - we should always have all channels... maybe this is over complicating?
        for ii,chan in enumerate(chans):
            # make subplot
            axes[ii] = fig.add_subplot(len(chans),1,ii+1)
            if ii == 0:
                axes[0].set_title('click to adjust fits - hold shift to force position')
            # find the data
            ndx = np.where(np.logical_and(mydata['element']==element, mydata['channel']==chan))[0]
            if len(ndx)>1:
                print('found more than one profile for %s, channel %d' % (element, chan))
                # show what is available, choose
                ndx = ndx[-1] # take the last one
            elif len(ndx)==0:
                print('No profile found for %s, channel %d' % (element, chan))
            else:
                ndx = ndx[0]
            # read the profile
            prof = np.loadtxt(mydata.iloc[ndx]['filename'])
            prof = prof[xdata,1]
            # plot this
            lines[ii] = axes[ii].plot(xdata, prof, label='%s ch %d' % (element, chan))
            axes[ii].legend(loc='right')
            axes[ii].set_ylim(0, prof.max()*1.2)
            # plot the starting points
            markers[ii] = axes[ii].plot(peaks_channel[ii, :], np.ones(len(peaks_channel[ii, :]))*prof.max(), 'r*')
            # try to fit based on the automatic values
            fitvalues, fitprofile = fitPeaks(xdata, prof, peaks_channel[ii,:])
            # add to the plot
            fits[ii] = axes[ii].plot(xdata, fitprofile, 'r-')
            # this gives us a first attempt
            peaks_channel_fitted[ii, :] = peaks_channel[ii,:] + fitvalues[1] - peaks_channel[ii, 0]
            # handle cases where we cant fit - to add (hold shift and click?)
            
        
        # interactive update of markers
        print('click on the profiles to adjust the starting point for the fits - when finished, click outside the axes')
        global myaxis
        global coords
        global shift
        coords = []
        myaxis = None
        shift = False
        plt.draw()
        while myaxis != -1:
            cid = fig.canvas.mpl_connect('button_press_event', clickfunc)
            cid2 = fig.canvas.mpl_connect('key_press_event', keypressfunc)
            cid2 = fig.canvas.mpl_connect('key_release_event', keyreleasefunc)
            print(cid)
            print('in update')
            if not plt.waitforbuttonpress():
                print('here I am... '+str(myaxis))
                if myaxis == -1:
                    # disconnect from the figure
                    print('disconnect from the figure')
                    fig.canvas.mpl_disconnect(cid)
                    break
                # modify peaks_channel
                offset = coords[0] - peaks_channel[myaxis, :]
                offset_abs = np.abs(offset)
                ndx = np.nonzero(offset_abs == offset_abs.min())[0]
                offset = offset[ndx]
                peaks_channel[myaxis,:] += offset
                # update the starting points
                markers[myaxis][0].set_xdata(peaks_channel[myaxis,:])
                # refit from this point
                prof = lines[myaxis][0].get_ydata()
                xdata = lines[myaxis][0].get_xdata()
                fitvalues, fitprofile = fitPeaks(xdata, prof, peaks_channel[myaxis,:], shift)
                # add to the plot
                fits[myaxis][0].set_ydata(fitprofile)
                plt.draw()
                # save this updated value
                peaks_channel_fitted[myaxis, :] = peaks_channel[myaxis,:] + fitvalues[1] - peaks_channel[myaxis, 0]
                # handle cases where we cant fit
        # update the dictionary
        fitted_data[element].update({'fit':peaks_channel_fitted})
    
    # close the figure
    plt.close(fig)
    
    # make the energy calibration
    calibE = makeCalibEnergy(fitted_data, order)   
    print('calibE = ...')
    print(calibE)     
    calib[:,1:4] = calibE
    return calib
    # end of the main function



def saveCalib(calib):
    f = open('FalconXcalib.txt', 'wt')
    f.write('channel, E0, E1, E2, A0, A1, z\n')
    print('channel, E0, E1, E2, A0, A1, z')
    for ii in range(calib.shape[0]):
        f.write('%d, %f, %f, %0.8f, %f, %f, %f\n' % (calib[ii,0], calib[ii,1], calib[ii,2], calib[ii,3], calib[ii,4], calib[ii,5], calib[ii,6]))
        print('%d, %f, %f, %f, %f, %f, %f' % (calib[ii,0], calib[ii,1], calib[ii,2], calib[ii,3], calib[ii,4], calib[ii,5], calib[ii,6]))
    f.close()
    
        
def fitPeaks(xdata, ydata, peaks, shift=False):
    # x is amp / pos / sigma, then amp / sigma for remaining peaks, then bkg
    x0 = np.ones(len(peaks)*2 + 2) * 3 # 3 is a good value for sigma
    x0[0] = ydata.max() - ydata.min() # amplitude first peak
    x0[1] = peaks[0] # position first peak
    x0[3:-1:2] = x0[0] # amplitude == first amplitude
    x0[-1] = ydata.min()
    if not shift:
        fitvalues = leastsq(optfunc, x0, args=(xdata, ydata, peaks))[0]
    else:
        fitvalues = x0
    # make the profile
    fitprofile = ydata - optfunc(fitvalues, xdata, ydata, peaks)
    return fitvalues, fitprofile

def optfunc(x, xdata, ydata, mypeaks, evalute=False):
    # x is amplitude, position, sigma for first peak,
    # then amplitude and sigma for subsequent peaks
    # plus background
    # fitted forcing the distance between lines to stay fixed
    guess = np.ones(len(xdata)) * x[-1] # flat background (last coefficient)
    # add first gaussian peak (first three coefficients)
    guess += x[0]*np.exp(-((xdata-x[1])**2)/(2*(x[2]**2)))
    # add remaining peaks
    for ii in range(1, len(mypeaks)):
        # fixed offset in position to first peak
        pos = x[1] + mypeaks[ii] - mypeaks[0]
        ndx = 3+(ii-1)*2
        guess += x[ndx]*np.exp(-((xdata-pos)**2)/(2*(x[ndx+1]**2)))
    # calculate difference
    dif = ydata - guess
    return dif

def makeCalibAngle(data, tth_nom, dist):
    # fit the angle and Z components
    z0 = np.array([-30,-20,-10,0,10,20,30])
    nchan = data.shape[0]
    x0 = np.zeros(nchan*2 + 2) # two angle params per channel, dz, dist
    x0[-1] = dist
    xfit = leastsq(optfuncNchannel, x0, args=(data, tth_nom, z0))[0]
    calibA = np.zeros((nchan, 3))
    calibA[:,0] = xfit[0:nchan]
    calibA[:,1] = xfit[nchan:(2*nchan)]
    calibA[:,2] = z0 + xfit[-2]
    print('distance should be %0.1f' % xfit[-1]) 
    return calibA
    
    
    
def optfuncNchannel(x, tth_obs, tth_nominal, z0, fitting=True):
    # x: A0, A1, z, 7 rows for 7 channels
    nchan = tth_obs.shape[0]
    A0 = x[0:nchan]
    A1 = x[nchan:(2*nchan)]
    dz = x[-2]
    dist = x[-1]
    # deal with z
    z = z0 + dz
    mytth = np.zeros(tth_obs.shape)
    for chan in range(nchan):
        for ntth,tth_nom in enumerate(tth_nominal):
            tth_theory = np.rad2deg(np.arccos(dist*np.cos(np.deg2rad(tth_nom)) / ((dist**2 + z[chan]**2)**0.5)))
            # deal with offset
            tth_offset = tth_nom*A0[chan] + A1[chan]
            # combine
            mytth[chan, ntth] = tth_theory + tth_offset
    dif = mytth - tth_obs
    return dif.flatten()


def makeCalibEnergy(data, order=1):
    # use the fitted data to prodce the calibration...
    elements = list(data.keys())
    for ii,elem in enumerate(elements):
        if ii == 0:
            # first element
            ydata = data[elem]['peaks'] # value in eV
            xdata = data[elem]['fit'] # position in channels 
        else:
            ydata = np.concatenate((ydata, data[elem]['peaks']))
            xdata = np.concatenate((xdata, data[elem]['fit']), axis=1)
    xdata+=1 #Laura
    # fitting
    ydata = ydata/1000 # for keV
    calibE = np.zeros((xdata.shape[0], 3)) # 3 coefficients
    print('fitting calibE, order = %d' % order)
    for chan in range(xdata.shape[0]):
        xfit,yfitted = fit_poly(xdata[chan,:], ydata, order)
        if order == 2:
            #calibE[chan,0:3] = xfit
            calibE[chan,1:3] = xfit #Laura
        elif order == 1:
            calibE[chan,0:2] = xfit
        else:
            print('order must be 1 or 2')
    return calibE

def fit_poly(xdata, ydata, order=2):
    # fit n order poly y = a*x**2 + b*x + c
    # adapted from fitting in tomodata_nexus
    if order==1:
        optfunc = lambda x: x[0] + x[1]*xdata - ydata
        x0 = np.zeros(order+1)
    elif order==2:
        print ("toto = " + str(xdata[0])+str(xdata[-1]))
        #optfunc = lambda x: x[0] + x[1]*xdata + x[2]*xdata**2 - ydata
        #Trying with fixing the terms to see if it improves the CAESAR
        optfunc = lambda x:  x[0]*xdata + x[1]*xdata**2 - ydata #Laura
        x0 = np.zeros(order)
    else:
        print("sorry, 3 is the limit ! ")
        return
    x1 = leastsq(optfunc, x0)[0]
    yfit = optfunc(x1) + ydata
    return x1, yfit

def keypressfunc(event):
    global shift
    if event.key == 'shift':
        shift = True
        print('shift key pressed')
    else:
        print('another key pressed')
    return shift

def keyreleasefunc(event):
    global shift
    if event.key == 'shift':
        shift = False
        print('shift key released')
    else:
        print('another key released')
    return shift


    
def clickfunc(event):
    global coords
    global myaxis
    coords = [event.xdata, event.ydata]
    myaxis = -1
    for ii,axis in enumerate(axes):
        if event.inaxes == axis:
            myaxis = ii
    print(coords)
    print(myaxis)
    return coords, myaxis

    
def parse_list(mylist):
    data = pandas.DataFrame()
    for name in mylist:
        elems = name.split('_')
        myelement = elems[0][5:]
        myangle = float(elems[1][0:2])
        mychan = int(elems[2][-2:])
        myndx = int(elems[3][0:4])
        info = pandas.DataFrame([myelement, myangle, mychan, myndx, name])
        data = pandas.concat((data, info), axis=1, ignore_index=True)
    data = data.T
    data = data.sort_values(by=[0,2,1,3], axis=0) # sort by element, chan, angle, ndx
    data = data.reset_index(drop=True)
    data = data.rename(columns={0:'element', 1:'angle', 2:'channel', 3:'ndx', 4:'filename'})
    return data

def getFluoData(element='Au'):
    # here we can code all the fluo peaks
    # values in eV, taken from Laura's function - in the same order at the moment...
    if element=='Mo':
        peaks = np.array([17444.3, 19608.3])
    elif element=='Sn':
        peaks = np.array([25271.3, 25044., 28486.])
    elif element=='Ba':
        peaks = np.array([32193., 31817.1, 36378.])
    elif element=='Sm':
        peaks = np.array([40118.1, 39522])
    elif element=='Au':
        peaks = np.array([68803.7, 66989.5])
    else:
        print('element %s not recognised!' % element)
        peaks = np.array([])
    return peaks

def energy2channel(peaks, calibE):
    peaks_out = np.zeros((calibE.shape[0], len(peaks)))
    for ii in range(len(peaks)):
        for jj in range(calibE.shape[0]):
            # energy calib is quadratic, need to slove
            c = calibE[jj,0] - (peaks[ii]/1000)
            b = calibE[jj,1]
            a = calibE[jj,2]
            if a != 0:
                chan_a = (-b + ((b**2) - (4*a*c))**0.5) / (2*a)
                chan_b = (-b - ((b**2) - (4*a*c))**0.5) / (2*a)
                # choose the good root!
                if np.abs(chan_a - 1024) > np.abs(chan_b - 1024):
                    chan = chan_b
                else:
                    chan = chan_a
            else:
                # quadratic ter is zero, simple solution
                chan = (peaks[ii]/1000 - a) / b
            peaks_out[jj,ii] = chan
    return peaks_out

def getDiffData(element):
    # get lattice parameter and hkls
    if element == 'Au':
        a = 4.0782 # laura = 4.0704 # angstrommake
        hkl = np.array(((1,1,1), (0,0,2), (2,2,0), (3,1,1), (2,2,2), (4,0,0), (4,2,0), (4,2,2)))
    elif element == 'LaB6':
        a = 4.155
        hkl = np.array(((0,0,1), (0,1,1), (1,1,1)))
    else:
        print('element %s not recognised!' % element)
        peaks = np.array([])       
    peaks = a / ((hkl**2).sum(1))**0.5
    return peaks, hkl
    
def select_peaks_dsp(peaks, hkl, calib, dist, tth_nom):
    # convert to dsp to energy, apply cropping in energy
    # need sample dectector distance (R + deltaR)
    # return the reduced list of peaks and hkls
    peaks_energy = np.zeros((calib.shape[0], len(peaks)))
    for jj in range(calib.shape[0]):
        tth = np.rad2deg(np.arccos(dist*np.cos(np.deg2rad(tth_nom)) / ((dist**2 + calib[jj,6]**2)**0.5)))
        tth_offset = tth_nom * calib[jj, 4] + calib[jj, 5]
        tth = tth + tth_offset
        theta = np.deg2rad(tth) / 2
        lam = 2 * peaks * np.sin(theta)
        energy = 1000 * 12.398 / lam # in eV to use energy2channel
        peaks_energy[jj,:] = energy / 1000 # back to keV
    # crop in energy to 30 - 75 keV
    ndx = np.nonzero(np.logical_and(peaks_energy.min(0)>30, peaks_energy.max(0)<75))[0]
    hkl = hkl[ndx,:]
    peaks = peaks[ndx]
    return peaks, hkl

def estimate_tth(tth_nom, calibA, dist):
    # this is the calculation of theta effective for one or more channels
    # calibA is the three last columns of calib
    tth = np.rad2deg(np.arccos(dist*np.cos(np.deg2rad(tth_nom)) / ((dist**2 + calibA[:,2]**2)**0.5)))
    tth_offset = tth_nom * calibA[:, 0] + calibA[:, 1]
    tth = tth + tth_offset
    return tth

def dsp2channel(dsp, tth, calibE):
    # for one channel only - give effective angle and the energy part of calib
    theta = np.deg2rad(tth / 2)
    lam = 2 * dsp * np.sin(theta)
    energy = 1000 * 12.398 / lam # in eV for energy2channel
    chan = energy2channel(energy, calibE.reshape(1,3))
    return chan
    
def optfunc2(x, dsp, calibE, xdata, ydata):
    # we are fitting theta (x = theta, amplitude,sigma per peak, then bkg )
    # dsp should have only the useful peaks
    # convert dsp to channel
    peaks_channel = dsp2channel(dsp, x[0], calibE)[0]
    # build the profile
    guess = np.ones(len(xdata)) * x[-1] # flat background (last coefficient)
    # add first gaussian peak (first three coefficients)
    for ii,pos in enumerate(peaks_channel):
        ndx = 2*ii + 1
        guess += x[ndx]*np.exp(-((xdata-pos)**2)/(2*(x[ndx+1]**2)))
    # calc difference
    dif = ydata - guess
    return dif

def fitPeaksAngle(tth_guess, peaks, calibE, xdata, ydata, shift=False):
    # x is angle, amp / sigma for all peaks, then bkg
    x0 = np.ones(len(peaks)*2 + 2) * 5 # 5 is a good value for sigma
    x0[0] = tth_guess
    x0[1:-1:2] = ydata.max() - ydata.min() # amplitude all peaks
    x0[-1] = ydata.min()
    if not shift:
        fitvalues = leastsq(optfunc2, x0, args=(peaks,calibE, xdata, ydata))[0]
    else:
        fitvalues = x0
    # make the profile
    fitprofile = ydata - optfunc2(fitvalues, peaks, calibE, xdata, ydata)
    return fitvalues, fitprofile


def modify_tth_guess(oldtth, newchan, oldchan, calibE):
    oldE = calibE[0] + calibE[1]*oldchan + calibE[2]*oldchan**2
    newE = calibE[0] + calibE[1]*newchan + calibE[2]*newchan**2
    newtth = oldtth * oldE / newE
    return newtth
    
def getDefaultCalib():
    calib = np.zeros((7, 7))
    calib[:,0] = np.arange(7)
    calib[:,2] = 0.05
    calib[:,6] = np.arange(-30, 31, 10)
    return calib
    
   
